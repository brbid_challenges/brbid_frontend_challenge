# Frontend Challenge

## Considerações Gerais

Você deverá fazer um fork deste projeto, todos os seus commits devem estar registrados nele, pois queremos ver como você trabalha.

## O desafio

Vamos dar uma olhada na previsão do tempo? A meta é criarmos uma página simples, que consuma a API do Yahoo de previsão do tempo (https://developer.yahoo.com/weather/). O layout final deverá ficar o mais próximo possível da versão abaixo (vamos ter a versão responsiva mobile e a versão desktop):

Web:

![](https://s3-sa-east-1.amazonaws.com/media-desafios/desktop01.png =300x)

![](https://s3-sa-east-1.amazonaws.com/media-desafios/desktop02.png =300x)

Mobile responsivo:

![alt tag](https://s3-sa-east-1.amazonaws.com/media-desafios/mobile01.png)

![alt tag](https://s3-sa-east-1.amazonaws.com/media-desafios/mobile02.png)

## Dicas

* Tudo bem, até pode usar jquery. Uma sugestão: Axios para a comunicação com a API.
* HTML o mais semântico possível.
* Branches com readme e instruções de implantação e como rodar seu projeto serão bem vindos.
* Pré-processadores CSS como Sass.
* Você pode utilizar o framework de SPA que tiver mais afinidade.

## Como entregar?

* Faça um fork deste projeto e nos envie o seu link

